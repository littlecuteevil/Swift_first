func findIndexWithGenerics<T: Equatable>(ofElement elementToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == elementToFind {
            return index
        }
    }
    return nil
}

 
let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
let arrayofDouble = [1, 1.2, 1.4, 1.6, 1.8, 10000.5]
let arrayofInt = [4, 2, 5, 2, 5, 6]

if let foundIndex = findIndexWithGenerics(ofElement: "кот", in: arrayOfString) {
    print("Индекс кота: \(foundIndex)")
}

if let foundIndex = findIndexWithGenerics(ofElement: 10000.5, in: arrayofDouble) {
    print("Индекс 10000.5: \(foundIndex)")
}

if let foundIndex = findIndexWithGenerics(ofElement: 5, in: arrayofInt) {
    print("Индекс 5: \(foundIndex)")
}

