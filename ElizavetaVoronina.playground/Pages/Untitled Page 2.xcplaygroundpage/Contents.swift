enum Priority: String {
    case high = "high"
    case middle = "middle"
    case low = "low"
}

//Класс (задание 1-4)
class ClassField {
    let header: String
    let length: Int
    var placeholder: String?
    let code: Int?
    let priority: String
    
    init(header: String, length: Int, placeholder: String? = nil, code: Int? = nil, priority: Priority) {
        self.header = header
        self.length = length
        self.priority = priority.rawValue
        self.code = code
        self.placeholder = placeholder
    }
    
    func setPlaceholder(with newPlaceholder: String) {
        self.placeholder = newPlaceholder
    }
    
    func ifLengthIsValid() -> Bool {
        return length <= 25
    }
}

//Структура (задание 5)
struct StructField {
    let header: String
    let length: Int
    var placeholder: String? = nil
    var code: Int? = nil
    
    mutating func setPlaceholder(with newPlaceholder: String) {
        self.placeholder = newPlaceholder
    }
    
    func ifLengthIsValid() -> Bool {
        return length <= 25
    }
}

//Инициализируем экземпляры класса ClassSield
let nameClassField = ClassField(header: "Name", length: 25, placeholder: "Type your name", code: 1, priority: Priority.high)
let surnameClassField = ClassField(header: "Surname", length: 25, placeholder: "Type your surname", code: 2, priority: Priority.high)
let ageClassField = ClassField(header: "Age", length: 3, code: 3, priority: Priority.middle)
let cityClassField = ClassField(header: "City", length: 15, placeholder: "City", priority: Priority.low)

nameClassField.setPlaceholder(with: "1234")
nameClassField.ifLengthIsValid()

//Инициализируем экземпляры структуры StructField
var nameStructField = StructField(header: "Name", length: 25, placeholder: "Type your name", code: 1)
let surnameStructField = StructField(header: "Surname", length: 25, placeholder: "Type your surname", code: 2)
let ageStructField = StructField(header: "Age", length: 3, code: 3)
let cityStructField = StructField(header: "City", length: 15, placeholder: "City")

nameStructField.setPlaceholder(with: "1234")
nameStructField.ifLengthIsValid()
