import Foundation


//Задание 1
func fibonacciNumber(n: Int) -> [Int] {
    switch n {
        case 0:
            return [0]
        case 1:
            return [0, 1]
        default:
            var storyPointsNumbers: [Int] = [0, 1]
            for i in 1...n-1 {
                let number = storyPointsNumbers[i] + storyPointsNumbers[i-1]
                storyPointsNumbers.append(number)
            }
            return storyPointsNumbers
    }
}

print(fibonacciNumber(n: 0))
print(fibonacciNumber(n: 1))
print(fibonacciNumber(n: 2))
print(fibonacciNumber(n: 3))
print(fibonacciNumber(n: 4))
print(fibonacciNumber(n: 5))
print(fibonacciNumber(n: 6))
print(fibonacciNumber(n: 7))
print(fibonacciNumber(n: 8))


//Задание 2
let printArrayFor: ([Int]) -> Void = { fibonacci -> Void in
    for i in 0...(fibonacci.count-1) {
        print(fibonacci[i])
    }
}

printArrayFor(fibonacciNumber(n: 0))
printArrayFor(fibonacciNumber(n: 1))
printArrayFor(fibonacciNumber(n: 2))
printArrayFor(fibonacciNumber(n: 3))
