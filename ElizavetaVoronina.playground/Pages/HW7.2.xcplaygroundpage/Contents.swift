enum Judge {
    case Pupa
    case Lupa
    case Accounting
}

//Я не знаю, что такое typealias, но я погуглила - прикольно. Надеюсь, суть поняла
typealias Result = [Judge: Double]
typealias NameResultDict = [String: Result]

var resultsDict: NameResultDict = [
    "Bolt":[Judge.Pupa:10.0, Judge.Lupa: 7.0, Judge.Accounting:9.0],
    "Sharapova":[Judge.Pupa:9.0, Judge.Lupa: 10.0, Judge.Accounting:9.0],
    "Ronaldo":[Judge.Pupa:10.0, Judge.Lupa: 9.0, Judge.Accounting:10.0]
]

//Посчитайте средний балл каждого участника соревнований с использованием цикла for-in и выведите полученную информацию на консоль.
for (sportsmen, result) in resultsDict {
    print("Спортсмену \(sportsmen):")
    var sum: Double = 0
    for (judge, estimation) in result {
        print("судья \(judge) поставил \(estimation) баллов")
        sum += estimation
    }
    let avarageSum = sum/Double(result.keys.count)
    print("cредний балл для спортсмена  - \(avarageSum) \n")
}
    
        
        
