//Реализуйте цикл для вывода четных чисел от 0 до 50. Каждое четное число распечатайте в консоль.
//for i in 1...50 {
//    if i % 2 == 0 {
//        print(i)
//    }
//}


//Преобразуйте цикл так, чтобы каждое четное число записывалось в массив.
var intArray = [Int]()
for i in 0...50 {
    if i % 2 == 0 {
        intArray.append(i)
    }
}

//Создайте новый цикл с использованием while так, чтобы в консоль выводились только первые 10 четных чисел из массива
//Добавила доп. проверку, чтобы не выходить за границы массива
var i = 0
while (i < 10 && i < intArray.count){
    print(intArray[i])
    i += 1
}
